# -*- coding: utf-8 -*-
#

import sqlalchemy
#建立连接
from sqlalchemy import create_engine
#engine = create_engine('sqlite:///:memory:', echo=True)
engine =create_engine('sqlite:///test.db', convert_unicode=True)
# 创建映射
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

from sqlalchemy import Column, Integer, String,SmallInteger
class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(SmallInteger)
    company = Column(String)
    password = Column(String)
    def __repr__(self):
        return "<User(name='%s',  company='%s', age='%s')>" % (
            self.name,self.company, self.age)

#创建Session
#from sqlalchemy.orm import sessionmaker
#Session = sessionmaker(bind=engine)
#Session.configure(bind=engine)
#db_session = Session()

from sqlalchemy.orm import scoped_session, sessionmaker
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))


test_users = [
('userA2',21,"A", "1123"),
('userA3',32,"A", "xxd"),
('userA4',18,"A", "1123"), 
('userB1',42,"B", "pwb134"),
('userB2',53,"B", "pwb234"),
('userC1',23,"C", "1123"),
('userC2',37,"C", "1123"),
('userD1',24,"D", "1123"),
('userD2',37,"D", "567"),
('userD4',24,"D", "116623"),
('userD5',37,"D", "114423")
  ]


def init_db(): #只执行一次
    import os
    db_file = "test.db"
    if os.path.exists(db_file) :
        os.remove(db_file)
    Base.metadata.create_all(engine) 
    for name, age, company, passwd in reversed(test_users):
        user = User(name=name, age=age, company=company, password=passwd)
        db_session.add(user)
    db_session.commit()    
    
if __name__ == "__main__":
    primary_keys =[]
    for item in User._sa_class_manager.attributes:
        print item.class_.__name__, item.key
        if item.primary_key: 
            primary_keys.append( item.key)
    
        
    init_db()
    #group
    db_session.query(User).group_by(User.company).all()
    #filter  in/not in
    #切片操作如果没有则返回[],不会抛出异常
    db_session.query(User).filter(User.name.in_(['userB2', 'userA3']))[2:6] 
    db_session.query(User).filter(~User.name.in_(['userA3', 'userB1', 'userC1'])).all()
    #filter == != 
    db_session.query(User).filter(User.name.like('%B%')).all()
    # and/or 
    query = db_session.query(User)
    from sqlalchemy import and_,or_
    query.filter(and_(User.name == 'userA4', User.company == 'A'))
    query.filter(or_(User.name == 'userA4', User.company == 'B')).all()
    #ordr by
    for instance in db_session.query(User).order_by(User.name): 
        print instance.name, instance.company  
    


