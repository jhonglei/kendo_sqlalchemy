# -*- coding: utf-8 -*-

from flask import Flask,request,Response
from flask import abort, redirect, url_for
app = Flask(__name__)

@app.route("/")
def index():
    return  redirect( url_for('static',filename='index.html'))

import json
from kendo_sqlalchemy import kendo_read, kendo_update,kendo_create,kendo_destroy
from database import db_session,User
from jquery_param import merge_structs,parse_key_pair

@app.route("/Users1/",methods =['GET','POST','PUT','DELETE'])
def user_oppp():
    if 'GET' == request.method:
        '''
          1, models in args : create new item
          2, args is {}: query
        '''
        #args = jquery_unparam(request.url)  
        q,total = kendo_read(request.url, db_session, User)
        data =[ {'id':item.id, 'name':item.name,'age':item.age, 'company': item.company} for item in q]
        return json.dumps( {'total':total, 'data':data})
        
    elif  request.method in ('POST','PUT' ,'DELETE'):
        if request.form:
            struct_list = [parse_key_pair(key, val) for key,val in request.form.iteritems()]
            result  = merge_structs(struct_list)  
            models = result.get('models')
            if not models:
                abort(400) #错误请求（Bad Request）
            if "POST" == request.method:
                db_items = kendo_update(models,db_session, User)
            elif 'PUT' ==request.method:
                db_items = kendo_create(models,db_session, User)
            else:
                db_items = kendo_destroy(models,db_session, User)
            data =[ {'id':item.id, 'name':item.name,'age':item.age, 'company': item.company} for item in db_items]
            return json.dumps( data )   
            

###关闭数据库连接
@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

if __name__ == "__main__":
    app.run()